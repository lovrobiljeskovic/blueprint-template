import { RequestContext } from '../requestContext';
import { BlockTimeOracle } from './blockTimeOracle';
import { GasOracle } from './gasOracle';
import { TokenMetadataOracle } from './tokenMetadataOracle';

export interface NetworkConfig {
  getNetwork(): any;

  getNetworkName(): string;

  getInitStartBlock(): number;

  // gets the user lp transactions subgraph urls
  isContractNameLookupEnabled(): boolean;

  getMainProviderUrl(): string;

  getGasOracle(context: RequestContext): GasOracle;

  getTokenMetadataOracle(context: RequestContext): TokenMetadataOracle;

  getBlockTimeOracle(context: RequestContext): BlockTimeOracle;
}
