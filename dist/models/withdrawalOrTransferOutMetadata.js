"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExitedMetadata = exports.WithdrawalOrTransferOutMetadata = void 0;
class WithdrawalOrTransferOutMetadata {
    constructor(value, exited) {
        this.value = value;
        this.exited = exited;
    }
}
exports.WithdrawalOrTransferOutMetadata = WithdrawalOrTransferOutMetadata;
class ExitedMetadata {
    constructor(dayData) {
        this.exitRatio = dayData.exitRatio.toNumber();
        this.exitedSessionLength = dayData.exitedSessionLength.toNumber();
        this.exitedCostUsd = dayData.exitedCostUsd.toNumber();
        this.exitedValueUsd = dayData.exitedValueUsd.toNumber();
        this.exitedNetGainUsd = dayData.exitedNetGainUsd.toNumber();
        this.exitedNetGainPct = dayData.exitedNetGainPct.toNumber();
        this.exitedCollectedIncomeAmount = dayData.exitedCollectedIncomeAmount.toNumber();
        this.exitedCollectedIncomeUsd = dayData.exitedCollectedIncomeUsd.toNumber();
        this.exitedIncomeAPY = dayData.exitedIncomeAPY.toNumber();
        this.exitedRoiVsHodlUsd = dayData.exitedRoiVsHodlUsd.toNumber();
        this.exitedRoiVsHodlPct = dayData.exitedRoiVsHodlPct.toNumber();
        this.exitedHodlValueUsd = dayData.exitedHodlValueUsd.toNumber();
        this.exitedIfHeldAllAmountEthValueUsd = dayData.exitedIfHeldAllAmountEthValueUsd.toNumber();
        this.exitedIfHeldAllAmountBtcValueUsd = dayData.exitedIfHeldAllAmountBtcValueUsd.toNumber();
        this.exitedTokens = dayData.dayDataTokenLedger.map((t) => new ExitedToken(t.tokenName, t.exitedTokenAmount.toNumber(), t.exitedIfHeldAmountToken.toNumber()));
    }
}
exports.ExitedMetadata = ExitedMetadata;
class ExitedToken {
    constructor(exitedTokenName, exitedTokenAmount, exitedIfHeldAmountToken) {
        this.exitedTokenName = exitedTokenName;
        this.exitedTokenAmount = exitedTokenAmount;
        this.exitedIfHeldAmountToken = exitedIfHeldAmountToken;
    }
}
