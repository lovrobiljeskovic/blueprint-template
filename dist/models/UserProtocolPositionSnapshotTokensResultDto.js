"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserProtocolPositionSnapshotTokensResultDto = void 0;
class UserProtocolPositionSnapshotTokensResultDto {
    constructor(tokenName, tokenAddress, tokenType, isReceiptToken, transactionValueUsd, netTokenAmount, decimals, tokenRawAmount, totalAmountDeposited, totalAmountWithdrawn, amountDepositedChange, amountWithdrawnChange, amountIncomeChange, tokenIncomeUsd, tokenPriceUsd, tokenPriceSource, tokenAmountIfExit, ifHeldAmountToken, ifHeldAllAmountToken) {
        this.tokenName = tokenName;
        this.tokenAddress = tokenAddress;
        this.tokenType = tokenType;
        this.isReceiptToken = isReceiptToken;
        this.transactionValueUsd = transactionValueUsd;
        this.netTokenAmount = netTokenAmount;
        this.decimals = decimals;
        this.tokenRawAmount = tokenRawAmount;
        this.totalAmountDeposited = totalAmountDeposited;
        this.totalAmountWithdrawn = totalAmountWithdrawn;
        this.amountDepositedChange = amountDepositedChange;
        this.amountWithdrawnChange = amountWithdrawnChange;
        this.amountIncomeChange = amountIncomeChange;
        this.tokenIncomeUsd = tokenIncomeUsd;
        this.tokenPriceUsd = tokenPriceUsd;
        this.tokenPriceSource = tokenPriceSource;
        this.tokenAmountIfExit = tokenAmountIfExit;
        this.ifHeldAmountToken = ifHeldAmountToken;
        this.ifHeldAllAmountToken = ifHeldAllAmountToken;
    }
}
exports.UserProtocolPositionSnapshotTokensResultDto = UserProtocolPositionSnapshotTokensResultDto;
