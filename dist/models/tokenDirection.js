"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TokenDirection = void 0;
var TokenDirection;
(function (TokenDirection) {
    TokenDirection["INPUT_TOKEN"] = "Input";
    TokenDirection["OUTPUT_TOKEN"] = "Output";
})(TokenDirection = exports.TokenDirection || (exports.TokenDirection = {}));
