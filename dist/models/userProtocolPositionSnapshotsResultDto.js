"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserProtocolPositionSnapshotsResultDto = void 0;
class UserProtocolPositionSnapshotsResultDto {
    constructor(operationType, blockNumber, date, timestamp, txHash, txFeeUsd, gasTokenAmount, isLiabilityPosition, transactionValueUsd, positionSharesAtBlock, basePositionShares, basePositionCostUsd, basePositionUnitCostUsd, positionUsdValueAtBlockIfExit, exitedCostUsd, exitedValueUsd, ifHeldAllAmountEth, ethPriceUsd, ethPriceSource, ifHeldAllAmountBtc, btcPriceUsd, btcPriceSource, isNewSession, isFullyExitedSession, tokenLedger) {
        this.operationType = operationType;
        this.blockNumber = blockNumber;
        this.date = date;
        this.timestamp = timestamp;
        this.txHash = txHash;
        this.txFeeUsd = txFeeUsd;
        this.gasTokenAmount = gasTokenAmount;
        this.isLiabilityPosition = isLiabilityPosition;
        this.transactionValueUsd = transactionValueUsd;
        this.positionSharesAtBlock = positionSharesAtBlock;
        this.basePositionShares = basePositionShares;
        this.basePositionCostUsd = basePositionCostUsd;
        this.basePositionUnitCostUsd = basePositionUnitCostUsd;
        this.positionUsdValueAtBlockIfExit = positionUsdValueAtBlockIfExit;
        this.exitedCostUsd = exitedCostUsd;
        this.exitedValueUsd = exitedValueUsd;
        this.ifHeldAllAmountEth = ifHeldAllAmountEth;
        this.ethPriceUsd = ethPriceUsd;
        this.ethPriceSource = ethPriceSource;
        this.ifHeldAllAmountBtc = ifHeldAllAmountBtc;
        this.btcPriceUsd = btcPriceUsd;
        this.btcPriceSource = btcPriceSource;
        this.isNewSession = isNewSession;
        this.isFullyExitedSession = isFullyExitedSession;
        this.tokenLedger = tokenLedger;
    }
}
exports.UserProtocolPositionSnapshotsResultDto = UserProtocolPositionSnapshotsResultDto;
