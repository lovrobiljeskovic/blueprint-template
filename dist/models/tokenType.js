"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TokenType = void 0;
var TokenType;
(function (TokenType) {
    TokenType["NON_RECEIPT"] = "Non Receipt";
    TokenType["RECEIPT"] = "Receipt";
    TokenType["INCOME"] = "Income";
})(TokenType = exports.TokenType || (exports.TokenType = {}));
