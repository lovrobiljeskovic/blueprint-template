"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityType = void 0;
var EntityType;
(function (EntityType) {
    EntityType["SNAPSHOT"] = "SNAPSHOT";
    EntityType["INTERVAL_DATA"] = "INTERVAL_DATA";
    EntityType["DEPENDENT_DATA"] = "DEPENDENT_DATA";
})(EntityType = exports.EntityType || (exports.EntityType = {}));
