"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const simpleBlueprint_1 = __importDefault(require("./simpleBlueprint"));
const ts_jest_1 = require("@golevelup/ts-jest");
const mockBlockByDateApi = (0, ts_jest_1.createMock)({
    getBlockFromTimestamp(timestamp, networkId) {
        return Promise.resolve(1);
    },
});
const context = (0, ts_jest_1.createMock)({
    getBlockByDateApi() {
        return mockBlockByDateApi;
    },
});
const mySimpleBlueprint = new simpleBlueprint_1.default(context);
(function () {
    return __awaiter(this, void 0, void 0, function* () {
        const userAddress = '0x0000000000000000000000000000000000000001';
        const fromBlock = 16989112;
        try {
            const txns = yield mySimpleBlueprint.getUserTransactions(context, [userAddress], fromBlock);
            console.log(txns);
            const operations = yield mySimpleBlueprint.classifyTransaction(context, txns.userTransactions[0]);
            console.log(operations);
        }
        catch (error) {
            console.error(error);
            process.exitCode = 1;
        }
    });
})();
