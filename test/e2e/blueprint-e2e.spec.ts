import { BlockbydateAPI } from '../../src/models/blockbydateAPI';
import { BlueprintContext } from '../../src/models/blueprintContext';
import { Blueprint } from '../../src/models/blueprintInterface';
import { PositionValueContext } from '../../src/models/positionValueContext';
import { RequestContext } from '../../src/requestContext';
import MySimpleBlueprint from '../../src/simpleBlueprint';
import { DeepMocked, createMock } from '@golevelup/ts-jest';
import { beforeAll, expect, jest, test } from '@jest/globals';

describe('blueprint e2e tests', () => {
  let blueprint: Blueprint;
  let context: BlueprintContext;
  let mockBlockByDateApi: BlockbydateAPI;

  beforeAll(() => {
    mockBlockByDateApi = createMock<BlockbydateAPI>({
      getBlockFromTimestamp(timestamp: number, networkId?: string): Promise<number> {
        return Promise.resolve(1);
      },
    });
    context = createMock<BlueprintContext>({
      getBlockByDateApi(): BlockbydateAPI {
        return mockBlockByDateApi;
      },
    });
    blueprint = new MySimpleBlueprint(context);
  });

  describe('blueprint implements required methods', () => {
    it('getUserTransactions and classifyTransaction return correct value', async () => {
      const startingBlock = 1;
      const userAddresses = ['test-wallet-1']; // This should be edited by the BP developer
      const userTxns = await blueprint.getUserTransactions(context, userAddresses, startingBlock);
      userTxns.userTransactions.forEach(async (txn) => {
        const classified = await blueprint.classifyTransaction(context, txn);
        expect(classified.length).toBeGreaterThanOrEqual(1);
      });
    });
  });
});
